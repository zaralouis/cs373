#!/usr/bin/env node

/* jshint
    esversion: 6
*/

// -------------
// Assertions.js
// -------------

// https://www.w3schools.com/nodejs/ref_assert.asp

"use strict";

const assert = require('assert');

function cycle_length (n) {
    assert(n > 0);
    let c = 0;
    while (n > 1) {
        if ((n % 2) === 0) {
            n = (n / 2);}
        else {
            n = (3 * n) + 1;}
        c += 1;}
    assert(c > 0);
    return c;}

function main () {
    console.log("Assertions.js");
    assert(cycle_length( 1) === 1);
    assert(cycle_length( 5) === 6);
    assert(cycle_length(10) === 7);}
    console.log("Done.");

main();

/*
% node Assertions.js
Assertions.js
assert.js:269
    throw err;
    ^

AssertionError [ERR_ASSERTION]: The expression evaluated to a falsy value:

  assert(c > 0)

    at cycle_length (/Users/downing/Dropbox/examples/javascript/Assertions.js:26:5)
    at main (/Users/downing/Dropbox/examples/javascript/Assertions.js:31:12)
    at Object.<anonymous> (/Users/downing/Dropbox/examples/javascript/Assertions.js:36:1)
    at Module._compile (internal/modules/cjs/loader.js:702:30)
    at Object.Module._extensions..js (internal/modules/cjs/loader.js:713:10)
    at Module.load (internal/modules/cjs/loader.js:612:32)
    at tryModuleLoad (internal/modules/cjs/loader.js:551:12)
    at Function.Module._load (internal/modules/cjs/loader.js:543:3)
    at Function.Module.runMain (internal/modules/cjs/loader.js:744:10)
    at startup (internal/bootstrap/node.js:238:19)
*/
